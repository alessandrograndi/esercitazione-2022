from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views.generic import CreateView


class UserRegistrationView(CreateView):
    model = get_user_model()
    form_class = UserCreationForm
    success_url = reverse_lazy("autonoleggio:lista-veicoli")

#    template_name = "autonoleggio/veicolo_create.html"

