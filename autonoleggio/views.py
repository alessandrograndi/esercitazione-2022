from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView, UpdateView

from autonoleggio.forms import VeicoloForm
from autonoleggio.models import Veicolo


class VeicoloListView(ListView):
    model = Veicolo


class VeicoloDetailView(DetailView):
    model = Veicolo


@method_decorator(login_required, name="dispatch")
class VeicoloCreateView(CreateView):
    model = Veicolo
    form_class = VeicoloForm
    success_url = reverse_lazy("autonoleggio:lista-veicoli")

    template_name = "autonoleggio/veicolo_create.html"


@method_decorator(login_required, name="dispatch")
class VeicoloUpdateView(UpdateView):
    model = Veicolo
    form_class = VeicoloForm
    success_url = reverse_lazy("autonoleggio:lista-veicoli")
    template_name = "autonoleggio/veicolo_update.html"
