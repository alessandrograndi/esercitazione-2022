from django.urls import path

from autonoleggio import views

app_name = "autonoleggio"

urlpatterns = [
    path("veicoli/", views.VeicoloListView.as_view(), name="lista-veicoli"),
    path(
        "veicolo/<int:pk>/",
        views.VeicoloDetailView.as_view(),
        name="dettaglio-veicolo"
    ),
    path(
        "veicolo/crea/",
        views.VeicoloCreateView.as_view(),
        name="nuovo-veicolo"
    ),
    path(
        "veicolo/<int:pk>/modifica/",
        views.VeicoloUpdateView.as_view(),
        name="modifica-veicolo"
    ),
]
