from django import forms

from autonoleggio.models import Veicolo


class VeicoloForm(forms.ModelForm):
    class Meta:
        model = Veicolo
        fields = (
            'tipo_di_veicolo',
            'numero_di_posti',
            'tariffa_giornaliera',
            'disponibile',
        )
        