from django.db import models

TIPI_DI_VEICOLO = [
    ("001", "Automobile"),
    ("002", "Motocicletta"),
    ("003", "Furgone"),
]


class Veicolo(models.Model):
    tipo_di_veicolo = models.CharField(max_length=255, choices=TIPI_DI_VEICOLO)
    numero_di_posti = models.PositiveIntegerField()
    tariffa_giornaliera = models.DecimalField(max_digits=10, decimal_places=2)
    disponibile = models.BooleanField(default=True)

    @property
    def descrizione_tipo_di_veicolo(self):
        return dict(TIPI_DI_VEICOLO).get(self.tipo_di_veicolo)

    def __str__(self):
        return f"{self.descrizione_tipo_di_veicolo} (id: {self.id})"
